package uk.ac.ebi.reaction.search.config;

import chemaxon.util.ConnectionHandler;
import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 *
 * @author joseph
 */

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class DataConfigIT {

    @Autowired
    private DataSourceProperties dataSourceProperties;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private ConnectionHandler connectionHandler;
    @Autowired
    private Connection connection;

    /**
     * Test of dataSourceProperties method, of class DataConfig.
     */
    @Test
    public void testDataSourceProperties() {
        assertThat(dataSourceProperties).isNotNull();
    }

    /**
     * Test of dataSource method, of class DataConfig.
     */
    @Test
    public void testDataSource() {
         assertThat(dataSource).isNotNull();
    }

    /**
     * Test of connectionHandler method, of class DataConfig.
     * @throws java.sql.SQLException
     */
    @Test
    public void testConnectionHandler() throws SQLException {
         assertThat(connectionHandler).isNotNull();
    }

    /**
     * Test of connection method, of class DataConfig.
     * @throws java.sql.SQLException
     */
    @Test
    public void testConnection() throws SQLException {
         assertThat(connection).isNotNull();
    }

}
