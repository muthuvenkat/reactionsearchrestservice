package uk.ac.ebi.reaction.search.restservice.model;

import java.sql.SQLException;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import uk.ac.ebi.reaction.search.dao.ReactionView;
import uk.ac.ebi.reaction.search.service.ApiService;
import uk.ac.ebi.reaction.search.util.QueryHelper;

/**
 * @author venkat
 * @date 23/04/2020
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
class SearcherTest {

    @Autowired
    private ApiService searcher;

    @Test
    void searchByText() throws SQLException, SQLException {

        assert (searcher.searchByText("RHEA:10001").size() >= 1);
        assert (searcher.searchByText("1.2.3.7").size() >= 2);
        assert (searcher.searchByText("CHEBI:27732").size() >= 12);
        assert (searcher.searchByText("10000").isEmpty());
        assertThat(searcher.searchByText("d-glucose")).isNotEmpty();

    }

    @Test
    void searchStructure() throws Exception {
        assert (searcher.searchStructure("Cn1cnc2n(C)c(=O)n(C)c(=O)c12", QueryHelper.TYPE_EXACT, null).size() == 1);
        assert (searcher.searchStructure("Cn1cnc2n(C)c(=O)n(C)c(=O)c12", QueryHelper.TYPE_SIMILARITY, "0.7").size() == 7);
        assert (searcher.searchStructure("O.[Fe++].[O-]S([O-])(=O)=O", QueryHelper.TYPE_SUBSTRUCTURE, null).isEmpty());
    }

    @Test
    void search() throws Exception {
        List<ReactionView> results = searcher.searchSimilarReactions("C(C[S+:5]([C:7])C)([O-])=O.C([O-])([C@](CC[S:15])"
                + "[N+])=O>>[O-]C(C[S:5]C)=O.[H+:16].O=C([C@](CC[S:15][C:7])[N+])[O-]", QueryHelper.TYPE_SIMILARITY, "0.7", QueryHelper.SIMILARITY_STRICT);

        assert (results.size() >= 2);

    }
}
