
package uk.ac.ebi.reaction.search.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author joseph
 */
@Entity
@Table(name = "rhea_chebi_xref")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RheaChebiXref.findAll", query = "SELECT r FROM RheaChebiXref r"),
    @NamedQuery(name = "RheaChebiXref.findByRheaId", query = "SELECT r FROM RheaChebiXref r WHERE r.rheaChebiXrefPK.rheaId = :rheaId"),
    @NamedQuery(name = "RheaChebiXref.findByChebiId", query = "SELECT r FROM RheaChebiXref r WHERE r.rheaChebiXrefPK.chebiId = :chebiId")})
public class RheaChebiXref implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RheaChebiXrefPK rheaChebiXrefPK;
    @JoinColumn(name = "rhea_id", referencedColumnName = "rhea_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ReactionApi reactionApi;

    public RheaChebiXref() {
    }

    public RheaChebiXref(RheaChebiXrefPK rheaChebiXrefPK) {
        this.rheaChebiXrefPK = rheaChebiXrefPK;
    }

    public RheaChebiXref(long rheaId, long chebiId) {
        this.rheaChebiXrefPK = new RheaChebiXrefPK(rheaId, chebiId);
    }

    public RheaChebiXrefPK getRheaChebiXrefPK() {
        return rheaChebiXrefPK;
    }

    public void setRheaChebiXrefPK(RheaChebiXrefPK rheaChebiXrefPK) {
        this.rheaChebiXrefPK = rheaChebiXrefPK;
    }

    public ReactionApi getReactionApi() {
        return reactionApi;
    }

    public void setReactionApi(ReactionApi reactionApi) {
        this.reactionApi = reactionApi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rheaChebiXrefPK != null ? rheaChebiXrefPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof RheaChebiXref)) {
            return false;
        }
        RheaChebiXref other = (RheaChebiXref) object;
        return !((this.rheaChebiXrefPK == null && other.rheaChebiXrefPK != null) || (this.rheaChebiXrefPK != null && !this.rheaChebiXrefPK.equals(other.rheaChebiXrefPK)));
    }

    @Override
    public String toString() {
        return "uk.ac.ebi.reaction.search.entity.RheaChebiXref[ rheaChebiXrefPK=" + rheaChebiXrefPK + " ]";
    }
    
}
