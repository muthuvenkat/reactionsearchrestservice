package uk.ac.ebi.reaction.search.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author joseph
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
@Table(name = "structure_api")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "StructureApi.findAll", query = "SELECT s FROM StructureApi s"),
    @NamedQuery(name = "StructureApi.findByCdId", query = "SELECT s FROM StructureApi s WHERE s.cdId = :cdId"),
    @NamedQuery(name = "StructureApi.findByAccession", query = "SELECT s FROM StructureApi s WHERE s.accession = :accession"),
    @NamedQuery(name = "StructureApi.findByRheaAsciiName", query = "SELECT s FROM StructureApi s WHERE s.rheaAsciiName = :rheaAsciiName")})
public class StructureApi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "cd_id")
    @EqualsAndHashCode.Include
    @ToString.Include
    @JsonIgnore
    private Integer cdId;
    @Column(name = "cd_structure", columnDefinition = "text")
    @JsonIgnore
    private String cdStructure;
    @Column(name = "accession")
    @ToString.Include
    private String accession;
    @Size(max = 4000)
    @Column(name = "rhea_ascii_name")
    @ToString.Include
    private String rheaAsciiName;

}
