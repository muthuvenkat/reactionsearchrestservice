package uk.ac.ebi.reaction.search.restservice.model;

/**
 * @author venkat
 * @date 21/04/2020
 */
public class QueryBean {

	private String query;

	private String coef;

	public String getQuery() {
		return this.query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getCoef() {
		return this.coef;
	}

	public void setCoef(String coef) {
		this.coef = coef;
	}
}
