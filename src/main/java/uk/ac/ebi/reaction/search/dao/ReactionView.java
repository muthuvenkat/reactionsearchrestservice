package uk.ac.ebi.reaction.search.dao;

import com.fasterxml.jackson.annotation.JsonIgnore;
import uk.ac.ebi.reaction.search.util.RxnUtil;

/**
 *
 * @author joseph
 */
public interface ReactionView {

    Integer getCdId();

    String getCdStructure();

    long getRheaId();

    String getInchi();

    @JsonIgnore
    default String getRxn() {

        return RxnUtil.getReactionFromRxnFile(getCdStructure());
    }

}
