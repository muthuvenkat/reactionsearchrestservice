package uk.ac.ebi.reaction.search.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author joseph
 */
@Data
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class RheaChebiXrefPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "rhea_id")
    private long rheaId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "chebi_id")
    private long chebiId;

}
