package uk.ac.ebi.reaction.search.service;

import chemaxon.jchem.db.DatabaseSearchException;
import chemaxon.jchem.db.JChemSearch;
import chemaxon.sss.SearchConstants;
import chemaxon.sss.search.JChemSearchOptions;
import chemaxon.util.ConnectionHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.ac.ebi.reaction.search.dao.ReactionView;
import uk.ac.ebi.reaction.search.dao.StructureView;
import uk.ac.ebi.reaction.search.repository.ReactionApiRepository;
import uk.ac.ebi.reaction.search.repository.StructureApiRepository;

/**
 *
 * @author joseph
 */
@Slf4j
@Service("apiService")
class ApiServiceImpl implements ApiService {

    public static final String TYPE_EXACT = "EXACT";
    public static final String TYPE_SIMILARITY = "SIMILARITY";
    public static final String TYPE_SUBSTRUCTURE = "SUBSTRUCTURE";
    public static final String REACTION_TABLE = "reaction";
    public static final String STRUCTURE_TABLE = "structure";
    private final static String INCHI = "Web-RInChIKey=";

    public final static String SIMILARITY_STRICT = "StrictReactionTanimoto";
    public final static String SIMILARITY_MEDIUM = "MediumReactionTanimoto";
    public final static String SIMILARITY_COARSE = "CoarseReactionTanimoto";
    public final static String SIMILARITY_REACTANT = "ReactantTanimoto";
    public final static String SIMILARITY_PRODUCT = "ProductTanimoto";

    private static final String RINCHI_PATH = "/nfs/public/ro/cm/sw/RInChI/bin/rinchi_cmdline/linux/x86_64/rinchi_cmdline";
    private static final String PREFIX_EC = "EC";
    private static final String PREFIX_CHEBI = "CHEBI:";
    private static final String PREFIX_RHEA = "RHEA:";

    private final ConnectionHandler connHandler;
    private final ReactionApiRepository apiRepository;
    private final StructureApiRepository structureApiRepository;

    @Autowired
    public ApiServiceImpl(ConnectionHandler connHandler, ReactionApiRepository apiRepository, StructureApiRepository structureApiRepository) {
        this.connHandler = connHandler;
        this.apiRepository = apiRepository;
        this.structureApiRepository = structureApiRepository;
    }

    @Override
    public List<ReactionView> searchByText(String query) {

        if (!query.startsWith(PREFIX_CHEBI)
                && !query.startsWith(PREFIX_EC)
                && !query.startsWith(PREFIX_RHEA)
                && query.chars().anyMatch(Character::isLetter)) {
            // resultList = searchStructuresByText(query);
            //why structure here

            List<Integer> cids = structureApiRepository.findStructureLike(query);
            if (!cids.isEmpty()) {
                return apiRepository.findRxnInChemAxon(cids);
            }
            

        }
        if (query.contains(".") || query.startsWith(PREFIX_EC)) {
            query = query.toUpperCase().replaceAll(PREFIX_EC, "").trim();
            return apiRepository.findRxnByEc(query);
        } else if (query.startsWith(PREFIX_CHEBI)) {
            query = query.toUpperCase().replaceAll(PREFIX_CHEBI, "").trim();
            long chebiId = Long.parseLong(query);

            return apiRepository.findRxnByChebiId(chebiId);
        } else {
            if (query.startsWith(PREFIX_RHEA)) {
                query = query.toUpperCase().replaceAll(PREFIX_RHEA, "").trim();
                long rheaId = Long.parseLong(query);
                return apiRepository.findRxnsByRheaId(rheaId);

            }

        }

        return Collections.EMPTY_LIST;
    }

    @Override
    public List<StructureView> searchStructure(String mol, String type, String f) {
        log.debug("Starting search Type: " + type);
        log.debug("Starting search MOL: " + mol);

        JChemSearch searcher = new JChemSearch(); // Create searcher object
        searcher.setQueryStructure(mol);
        searcher.setConnectionHandler(connHandler);
        searcher.setStructureTable(STRUCTURE_TABLE);
        Float simCoef = null;
        int searchType = 0;

        if (type.equalsIgnoreCase(TYPE_EXACT)) {
            searchType = SearchConstants.FULL;
        } else if (type.equalsIgnoreCase(TYPE_SIMILARITY)) {
            searchType = SearchConstants.SIMILARITY;
            if (f != null && !f.isEmpty()) {
                simCoef = 1 - Float.parseFloat(f);
            }
        } else {
            searchType = SearchConstants.SUBSTRUCTURE;
        }

        JChemSearchOptions searchOptions = new JChemSearchOptions(searchType);
        if (f != null && !f.isEmpty()) {
            searchOptions.setDissimilarityThreshold(simCoef);
        }

        searcher.setSearchOptions(searchOptions);
        try {
            searcher.run();
        } catch (IOException | DatabaseSearchException | SQLException ex) {
            log.error(ex.getMessage());
        }

        log.debug("TOTAL COUNT: " + searcher.getResultCount());

        List<Integer> cdIds = Arrays.stream(searcher.getResults())
                .boxed()
                .collect(Collectors.toList());

        if (!cdIds.isEmpty()) {
            return structureApiRepository.findStructureInCdId(cdIds);
        }
        return new ArrayList<>();

    }

    @Override
    public List<ReactionView> searchRxn(String term, String type, String coef, String simType) {
        log.debug("Searching reactions...");

        if (term.contains("//////")) {
            term = term.replaceAll("//////", "\n");
        }

        int count = 1;
        StringBuilder sb = new StringBuilder();
        for (String line : term.split("\n")) {

            if (count == 5) {
                line = line.substring(0, 6);
            }
            count++;
            sb.append(line).append("\n");
        }

        term = sb.toString();

        try {

            if (type.equalsIgnoreCase(TYPE_SIMILARITY)) {
                return searchSimilarReactions(term, type, coef, simType);
            } else {
                String inchi = getRInChI(term);
                return searchExactReaction(inchi);
            }
        } catch (Exception e) {
            log.error(e.getLocalizedMessage());
        }

        return new ArrayList<>();

    }

    @Override
    public List<ReactionView> searchSimilarReactions(String mol, String type, String f, String simType) {
        log.debug("Starting searchSimilarReactions Type: " + type);
        log.debug("Starting searchSimilarReactions MOL: " + mol);

        JChemSearch searcher = new JChemSearch(); // Create searcher object
        searcher.setQueryStructure(mol);
        searcher.setConnectionHandler(connHandler);
        searcher.setStructureTable(REACTION_TABLE);
        int searchType = 0;
        Float simCoef = 0.7F; //default

        searchType = SearchConstants.SIMILARITY;
        if (f != null) {
            simCoef = 1 - Float.parseFloat(f);
        }

        JChemSearchOptions searchOptions = new JChemSearchOptions(searchType);
        searchOptions.setDissimilarityThreshold(simCoef);
        searchOptions.setDissimilarityMetric(simType);
        searcher.setSearchOptions(searchOptions);
        try {
            searcher.run();
        } catch (IOException | DatabaseSearchException | SQLException ex) {
            log.error(ex.getMessage());
        }

        List<Integer> cdIds = Arrays.stream(searcher.getResults())
                .boxed()
                .distinct()
                .collect(Collectors.toList());

        return apiRepository.findRxnInChemAxon(cdIds);

    }

    private List<ReactionView> searchExactReaction(String query) {

        //must validate query to ensure only inchi is passed here
        return apiRepository.findRxnsByInchi(query);

    }

    private String getRInChI(String query) {

        String s;
        Process p;
        String result = "";
        try {

            File tempRxnFile = File.createTempFile(String.valueOf(System.nanoTime()), ".rxn");
            FileOutputStream fileStream = new FileOutputStream(tempRxnFile);
            try (OutputStreamWriter writer = new OutputStreamWriter(fileStream, "UTF-8")) {
                writer.write(query);
            }

            p = Runtime.getRuntime().exec(RINCHI_PATH + "  " + tempRxnFile);
            try (BufferedReader br = new BufferedReader(
                    new InputStreamReader(p.getInputStream(), StandardCharsets.UTF_8))) {
                while ((s = br.readLine()) != null) {
                    if (s.contains(INCHI)) {
                        result = s.replaceAll(INCHI, "");
                    }
                }
                p.waitFor();
                p.destroy();
                boolean delete = tempRxnFile.delete();
                if (!delete) {
                    log.error(tempRxnFile + " not deleted");
                }
            }
        } catch (IOException | InterruptedException e) {
            log.error(e.getLocalizedMessage());
        }
        return result;
    }

}
