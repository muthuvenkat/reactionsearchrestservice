package uk.ac.ebi.reaction.search;

import chemaxon.license.LicenseManager;
import chemaxon.license.LicenseProcessingException;
import java.io.FileNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

/**
 *
 * @author joseph
 */
@Slf4j
@Component
public class ApiLicenseManager implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        loadChemAxonLicense();
    }

    private void loadChemAxonLicense() {
        try {
            String licensePath = ResourceUtils.getFile("classpath:license.cxl").getPath();

            LicenseManager.setLicenseFile(licensePath);

        } catch (FileNotFoundException | LicenseProcessingException ex) {
            log.error(ex.getLocalizedMessage());
        }
    }

}
