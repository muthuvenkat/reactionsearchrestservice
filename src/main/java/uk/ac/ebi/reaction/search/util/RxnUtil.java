package uk.ac.ebi.reaction.search.util;

import chemaxon.formats.MolExporter;
import chemaxon.formats.MolImporter;
import chemaxon.struc.RxnMolecule;
import com.chemaxon.mapper.AutoMapper;
import com.chemaxon.mapper.Mapper;
import java.io.IOException;
import javax.validation.constraints.NotNull;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author joseph
 */
@Slf4j
public final class RxnUtil {

    private static final String MOL = "mol";

    private RxnUtil() {
    }

    public static String getReactionFromRxnFile(@NotNull String rxnFile) {
        String result = rxnFile;
        try {
            AutoMapper mapper = new AutoMapper();
            RxnMolecule reaction = RxnMolecule.getReaction(MolImporter.importMol(rxnFile));
            mapper.setMappingStyle(Mapper.MappingStyle.CHANGING);
            mapper.map(reaction);
            result = MolExporter.exportToFormat(reaction, MOL);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage());
        }
        return result;
    }

}
