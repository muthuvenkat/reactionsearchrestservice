package uk.ac.ebi.reaction.search.restservice.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uk.ac.ebi.reaction.search.dao.ReactionView;
import uk.ac.ebi.reaction.search.dao.StructureView;
import uk.ac.ebi.reaction.search.restservice.model.QueryBean;
import uk.ac.ebi.reaction.search.service.ApiService;
import uk.ac.ebi.reaction.search.util.QueryHelper;

/**
 *
 * @author joseph
 */
//@CrossOrigin()
@RestController
@RequestMapping("/search")
public class ApiController {

    @Autowired
    private ApiService apiService;

    //added just for quick testing only. Please delete 
    @Deprecated
    @GetMapping(value = "/rxn", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReactionView> test() throws Exception {
        String query = "CHEBI:27732";
        return apiService.searchByText(query);
    }

    @GetMapping(value = "/rxn/text", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ReactionView> searchReactionByTerm(@RequestParam(value = "query") String query) throws Exception {
        return apiService.searchByText(query);
    }

    @PostMapping(value = "/rxn/exact", consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<ReactionView> searchExactReactions(@RequestBody String rxnFile) throws Exception {
        return apiService.searchRxn(rxnFile, QueryHelper.TYPE_EXACT, null, null);
    }

    @PostMapping(value = "rxn/similar/strict", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ReactionView> searchStrictSimReactions(@RequestBody QueryBean QueryBean) throws Exception {
        return apiService.searchRxn(QueryBean.getQuery(), QueryHelper.TYPE_SIMILARITY, QueryBean.getCoef(), QueryHelper.SIMILARITY_STRICT);
    }

    @PostMapping(value = "rxn/similar/medium", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ReactionView> searchMediumSimReactions(@RequestBody QueryBean QueryBean) throws Exception {
        return apiService.searchRxn(QueryBean.getQuery(), QueryHelper.TYPE_SIMILARITY, QueryBean.getCoef(), QueryHelper.SIMILARITY_MEDIUM);
    }

    @PostMapping(value = "rxn/similar/coarse", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ReactionView> searchCoarseSimReactions(@RequestBody QueryBean QueryBean) throws Exception {
        return apiService.searchRxn(QueryBean.getQuery(), QueryHelper.TYPE_SIMILARITY, QueryBean.getCoef(), QueryHelper.SIMILARITY_COARSE);
    }

    @PostMapping(value = "rxn/similar/product", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ReactionView> searchProductSimReactions(@RequestBody QueryBean QueryBean) throws Exception {
        return apiService.searchRxn(QueryBean.getQuery(), QueryHelper.TYPE_SIMILARITY, QueryBean.getCoef(), QueryHelper.SIMILARITY_PRODUCT);
    }

    @PostMapping(value = "rxn/similar/reactant", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<ReactionView> searchReactantSimReactions(@RequestBody QueryBean QueryBean) throws Exception {
        return apiService.searchRxn(QueryBean.getQuery(), QueryHelper.TYPE_SIMILARITY, QueryBean.getCoef(), QueryHelper.SIMILARITY_REACTANT);
    }

    @PostMapping(value = "/structure/exact", consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<StructureView> searchExactStructure(@RequestBody String molFile) throws Exception {
        return apiService.searchStructure(molFile, QueryHelper.TYPE_EXACT, null);
    }

    @PostMapping(value = "/structure/substructure", consumes = MediaType.TEXT_PLAIN_VALUE)
    public List<StructureView> searchSubStructure(@RequestBody String molFile) throws Exception {
        return apiService.searchStructure(molFile, QueryHelper.TYPE_SUBSTRUCTURE, null);
    }

    @PostMapping(value = "/structure/similar", consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<StructureView> searchSimilarStructure(@RequestBody QueryBean queryBean) throws Exception {
        return apiService.searchStructure(queryBean.getQuery(), QueryHelper.TYPE_SIMILARITY, queryBean.getCoef());
    }

}
