package uk.ac.ebi.reaction.search.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uk.ac.ebi.reaction.search.dao.ReactionView;
import uk.ac.ebi.reaction.search.entity.ReactionApi;

/**
 *
 * @author joseph
 */
@Repository
public interface ReactionApiRepository extends JpaRepository<ReactionApi, Long> {

    @Query(value = "select cd_id as cdId, encode(cd_structure,'escape') as cdStructure, rhea_id as rheaId, inchi as inchi from reaction_api rx where rx.rhea_id in ( select rc1. rhea_id from rhea_chebi_xref rc1 where rc1.chebi_id = :chebi_id)", nativeQuery = true)
    List<ReactionView> findRxnByChebiId(@Param("chebi_id") long chebiId);

    @Query(value = "select cd_id as cdId, encode(cd_structure,'escape') as cdStructure, rhea_id as rheaId, inchi as inchi from reaction_api rx where rx.rhea_id in ( select re1.rhea_id from rhea_ec_xref re1 where re1.ec_no LIKE :ec_no )", nativeQuery = true)
    List<ReactionView> findRxnByEc(@Param("ec_no") String ec);

    @Query(value = "select cd_id as cdId, encode(cd_structure,'escape') as cdStructure, rhea_id as rheaId, inchi as inchi from reaction_api rx where :rhea_id in (rx.rhea_id, rx.parent_id, rx.bidirectionalid )", nativeQuery = true)
    List<ReactionView> findRxnsByRheaId(@Param("rhea_id") long rheaId);

    @Query(value = "select cd_id as cdId, encode(cd_structure,'escape') as cdStructure, rhea_id as rheaId, inchi as inchi from reaction_api rx where :rhea_id in (rx.rhea_id, rx.parent_id, rx.bidirectionalid )", nativeQuery = true)
    ReactionView findRxnByRheaId(@Param("rhea_id") long rheaId);

    @Query(value = "SELECT cd_id as cdId, encode(cd_structure,'escape') as cdStructure, rhea_id as rheaId, inchi as inchi from reaction WHERE rhea_id= :rhea_id ", nativeQuery = true)
    ReactionView findReactionByRheaId(@Param("rhea_id") long rheaId);

    @Query(value = "select cd_id as cdId, encode(cd_structure,'escape') as cdStructure, rhea_id as rheaId, inchi as inchi from reaction_api where cd_id in (:cd_id)", nativeQuery = true)
    List<ReactionView> findRxnInChemAxon(@Param("cd_id") List<Integer> cdId);

    @Query(value = "select cd_id as cdId, encode(cd_structure,'escape') as cdStructure, rhea_id as rheaId, inchi as inchi from reaction_api where inchi= :inchi", nativeQuery = true)
    List<ReactionView> findRxnsByInchi(@Param("inchi") String inchi);

}
