package uk.ac.ebi.reaction.search.util;

/**
 *
 * @author joseph
 */
public class QueryHelper {

    public static final String TYPE_EXACT = "EXACT";
    public static final String TYPE_SIMILARITY = "SIMILARITY";
    public static final String TYPE_SUBSTRUCTURE = "SUBSTRUCTURE";
    public static final String REACTION_TABLE = "reaction";
    public static final String STRUCTURE_TABLE = "structure";
    private final static String INCHI = "Web-RInChIKey=";

    public final static String SIMILARITY_STRICT = "StrictReactionTanimoto";
    public final static String SIMILARITY_MEDIUM = "MediumReactionTanimoto";
    public final static String SIMILARITY_COARSE = "CoarseReactionTanimoto";
    public final static String SIMILARITY_REACTANT = "ReactantTanimoto";
    public final static String SIMILARITY_PRODUCT = "ProductTanimoto";

    private static final String ID = "id";
    private static final String NAME = "name";
    private static final String COEFF = "coeff";
    private static final String RINCHI_PATH = "/nfs/public/ro/cm/sw/RInChI/bin/rinchi_cmdline/linux/x86_64/rinchi_cmdline";
    private static final String PREFIX_EC = "EC";
    private static final String PREFIX_CHEBI = "CHEBI:";
    private static final String PREFIX_RHEA = "RHEA:";
    private static final String MOL = "mol";
    private static final String RXN_FILE = "rxnFile";
    private static final String CHEBI_IDS = "chebiIds";
    private static final String EC_NOS = "ecNos";

    private QueryHelper() {
    }

}
