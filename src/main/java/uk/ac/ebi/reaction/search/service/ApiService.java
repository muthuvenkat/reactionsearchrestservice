package uk.ac.ebi.reaction.search.service;

import java.util.List;
import uk.ac.ebi.reaction.search.dao.ReactionView;
import uk.ac.ebi.reaction.search.dao.StructureView;

/**
 *
 * @author joseph
 */
public interface ApiService {

    List<ReactionView> searchByText(String query);

    List<StructureView> searchStructure(String mol, String type, String f);

    List<ReactionView> searchRxn(String term, String type, String coef, String simType);

    List<ReactionView> searchSimilarReactions(String mol, String type, String f, String simType);
}
