package uk.ac.ebi.reaction.search.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author joseph
 */
@Entity
@Table(name = "rhea_ec_xref")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RheaEcXref.findAll", query = "SELECT r FROM RheaEcXref r"),
    @NamedQuery(name = "RheaEcXref.findByEcNo", query = "SELECT r FROM RheaEcXref r WHERE r.rheaEcXrefPK.ecNo = :ecNo"),
    @NamedQuery(name = "RheaEcXref.findByRheaId", query = "SELECT r FROM RheaEcXref r WHERE r.rheaEcXrefPK.rheaId = :rheaId")})
public class RheaEcXref implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RheaEcXrefPK rheaEcXrefPK;
    @JoinColumn(name = "rhea_id", referencedColumnName = "rhea_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ReactionApi reactionApi;

    public RheaEcXref() {
    }

    public RheaEcXref(RheaEcXrefPK rheaEcXrefPK) {
        this.rheaEcXrefPK = rheaEcXrefPK;
    }

    public RheaEcXref(String ecNo, long rheaId) {
        this.rheaEcXrefPK = new RheaEcXrefPK(ecNo, rheaId);
    }

    public RheaEcXrefPK getRheaEcXrefPK() {
        return rheaEcXrefPK;
    }

    public void setRheaEcXrefPK(RheaEcXrefPK rheaEcXrefPK) {
        this.rheaEcXrefPK = rheaEcXrefPK;
    }

    public ReactionApi getReactionApi() {
        return reactionApi;
    }

    public void setReactionApi(ReactionApi reactionApi) {
        this.reactionApi = reactionApi;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (rheaEcXrefPK != null ? rheaEcXrefPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof RheaEcXref)) {
            return false;
        }
        RheaEcXref other = (RheaEcXref) object;
        return !((this.rheaEcXrefPK == null && other.rheaEcXrefPK != null) || (this.rheaEcXrefPK != null && !this.rheaEcXrefPK.equals(other.rheaEcXrefPK)));
    }

    @Override
    public String toString() {
        return "uk.ac.ebi.reaction.search.entity.RheaEcXref[ rheaEcXrefPK=" + rheaEcXrefPK + " ]";
    }

}
