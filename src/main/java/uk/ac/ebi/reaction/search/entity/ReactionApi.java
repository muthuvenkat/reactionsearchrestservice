package uk.ac.ebi.reaction.search.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Singular;
import lombok.ToString;
import uk.ac.ebi.reaction.search.util.RxnUtil;

/**
 *
 * @author joseph
 */
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(onlyExplicitlyIncluded = true)
@NoArgsConstructor
@Entity
@Table(name = "reaction_api")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReactionApi.findAll", query = "SELECT r FROM ReactionApi r"),
    @NamedQuery(name = "ReactionApi.findByCdId", query = "SELECT r FROM ReactionApi r WHERE r.cdId = :cdId"),
    @NamedQuery(name = "ReactionApi.findByRheaId", query = "SELECT r FROM ReactionApi r WHERE r.rheaId = :rheaId"),
    @NamedQuery(name = "ReactionApi.findByInchi", query = "SELECT r FROM ReactionApi r WHERE r.inchi = :inchi"),
    @NamedQuery(name = "ReactionApi.findByParentId", query = "SELECT r FROM ReactionApi r WHERE r.parentId = :parentId"),
    @NamedQuery(name = "ReactionApi.findByBidirectionalid", query = "SELECT r FROM ReactionApi r WHERE r.bidirectionalid = :bidirectionalid")})
public class ReactionApi implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cd_id")
    private int cdId;
    @Column(name = "cd_structure", columnDefinition = "text")
    private String cdStructure;
    @ToString.Include
    @EqualsAndHashCode.Include
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "rhea_id")
    private long rheaId;
    @Size(max = 100)
    @Column(name = "inchi")
    private String inchi;
    @Column(name = "parent_id")
    private long parentId;
    @Column(name = "bidirectionalid")
    private long bidirectionalid;
    @Singular
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reactionApi", fetch = FetchType.EAGER)
    private Set<RheaChebiXref> rheaChebiXrefList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reactionApi", fetch = FetchType.EAGER)
    @Singular
    private Set<RheaEcXref> rheaEcXrefList;

    @JsonIgnore
    public String getRxnFile() {
        return RxnUtil.getReactionFromRxnFile(getCdStructure());

    }

}
