package uk.ac.ebi.reaction.search.swagger;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.servers.Server;
import java.util.Arrays;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author joseph
 */
@Configuration
public class SpringDocOpenApiConfig {

    @Bean
    public OpenAPI openAPI() {

        //TODO info here needs to be externalised in a property file
        Contact contact = new Contact();
        contact.setName("Venkat");
        contact.setEmail("venkat@ebi.ac.uk");

        License license = new License();
        license.setName("Apache 2.0");
        license.setUrl("https://www.apache.org/licenses/LICENSE-2.0.html");

        Server localhost = new Server();
        localhost.setDescription("Localhost");
        localhost.setUrl("http://localhost:8080/reaction/");

        Server devServer = new Server();
        devServer.setDescription("Development Server");
        devServer.setUrl("https://wwwdev.ebi.ac.uk/reaction/");

        return new OpenAPI()
                .info(new Info()
                        .title("Reaction Searching API")
                        .description("Reaction Searching API reference for developers")
                        .version("1.0")
                        .termsOfService("http://www.ebi.ac.uk")
                        .license(license)
                        .contact(contact))
                .servers(Arrays.asList(localhost, devServer));
    }

}
