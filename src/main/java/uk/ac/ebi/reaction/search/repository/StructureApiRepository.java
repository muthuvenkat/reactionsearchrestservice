package uk.ac.ebi.reaction.search.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uk.ac.ebi.reaction.search.dao.StructureView;
import uk.ac.ebi.reaction.search.entity.StructureApi;

/**
 *
 * @author joseph
 */
@Repository
public interface StructureApiRepository extends JpaRepository<StructureApi, Long> {

    @Query(value = "SELECT cd_id from structure_api WHERE lower(rhea_ascii_name) like :rhea_ascii_name", nativeQuery = true)
    List<Integer> findStructureLike(@Param("rhea_ascii_name") String name);

    //use this if cd_structure is uncommented in the structureView//@Query(value = "SELECT cd_id as cdId,encode(cd_structure,'escape') as cdStructure, accession as accession, rhea_ascii_name as rheaAsciiName  from structure_api WHERE cd_id in (:cd_id)", nativeQuery = true)
    @Query(value = "SELECT cd_id as cdId, accession as accession, rhea_ascii_name as rheaAsciiName  from structure_api WHERE cd_id in (:cd_id)", nativeQuery = true)
    List<StructureView> findStructureInCdId(@Param("cd_id") List<Integer> cdId);
}
