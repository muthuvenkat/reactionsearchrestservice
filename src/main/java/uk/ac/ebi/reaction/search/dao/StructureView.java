package uk.ac.ebi.reaction.search.dao;

/**
 *
 * @author joseph
 */
public interface StructureView {

    Integer getCdId();

    //uncomment and modify query if needed in the future
    //String getCdStructure();
    
    String getAccession();

    String getRheaAsciiName();

}
